const teas = require('../data/teas.json')

function getTeas(req, res, teaID) {
  if (!teaID) {
    return res.send(teas)
  }
  const tea = teas.find((tea) => tea.id == teaID )
  if (tea) {
    return res.send(tea)
  }
  throw new Error('the tea you requested was not found')
}

module.exports = getTeas