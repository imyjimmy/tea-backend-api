
create the serverless express proxy wrapper
`claudia generate-serverless-express-proxy --express-module app`

deploy it
`claudia create --handler lambda.handler --deploy-proxy-api --region us-east-1`