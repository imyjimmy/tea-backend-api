'use strict'

const getTeas = require('./handlers/get_teas')
const baseResp = require('./handlers/get_base') // should it be called getBase?
const createOrder = require('./handlers/create_order')
const modifyOrder = require('./handlers/put_order')
const deleteOrder = require('./handlers/delete_order')
const { getOrders, getOrder } = require('./handlers/get_order')
const server_resp = { success: 201, error: 400 }

const express = require('express')
const app = express()

var router = express.Router()

router.get('/', (req, res) => res.send('Hello World'))

router.route('/teas')
 
  .get( (req, res) => {
    return getTeas(req, res)
  })

router.route('/teas/:id')
  
  .get( (req, res) => {
    return getTeas(req, res, req.params.id)
  })

app.use('/', router) // app.use('/api', router) also a good idea

const port = process.env.PORT || 3000

module.exports = app