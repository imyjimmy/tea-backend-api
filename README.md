## AWS
aws user:
claudiajs-manager

.aws/credentials:
```
[claudiajs_manager]
aws_access_key_id = \*\*\*\*\*\*
aws_secret_access_key = \*\*\*\*\*\*
```

~/.bash_profile:
```
AWS_PROFILE=claudiajs_manager
```

## Claudia
### Generate the Express Lambda App

Create the serverless express proxy wrapper
`claudia generate-serverless-express-proxy --express-module app`

Deploy it
`claudia create --handler lambda.handler --deploy-proxy-api --region us-east-1`

After running `claudia update`:

```
loading Lambda config   lambda.getFunctionConfiguration FunctionName=website-bacpackaging files npm install -q --no-audit --production
npm WARN website-backend-api@0.0.0 No repository field.

updating REST API   apigateway.createDeployment restApiId=1h9hhkccjk    supdating REST API  apigateway.setAcceptHeader
{
  "FunctionName": "website-backend-api",
  "FunctionArn": 
  ...
  "url": "https://1h9hhkccjk.execute-api.us-east-1.amazonaws.com/latest"
}
```

The way to run locally is to `node app.local.js`

## DB
Create instance of DynamoDB:

`aws dynamodb create-table --table-name customer-orders --attribute-definitions AttributeName=orderId,AttributeType=S --key-schema AttributeName=orderId,KeyType=HASH --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 --region us-east-1 --query TableDescription.TableArn --output text`

Tables
DB Service | Table Name | Attribute Name | Attribute Type | Key Schema | Key Type
DynamoDB | customer-orders | orderId | String | orderId | Hash
etc

Scan Table:

`aws dynamodb scan --table-name ${table-name} --region us-east-1 --output json`
