# Creating a User Pool
## Output: pool id
website-backend-api (aws-cognito) $ aws cognito-idp create-user-pool --region us-east-1 --pool-name IndieTea --policies "PasswordPolicy={MinimumLength=8,RequireUppercase=false,RequireLowercase=false,RequireNumbers=false,RequireSymbols=false}" --username-attributes email --query UserPool.Id --output text
us-east-1_sRJMVrgoX

# Adding a client to the user pool
aws cognito-idp create-user-pool-client --region us-east-1 --user-pool-id us-east-1_sRJMVrgoX --client-name IndieTeaClient --no-generate-secret --query UserPoolClient.ClientId --output text
## Output: client id
2uauvigoq93bek7km88s587a74

# Adding a Facebook App
see developers.facebook.com
Facebook App Id: 1965964200166925

# Creating an Identity Pool

aws cognito-identity create-identity-pool --region us-east-1 --identity-pool-name IndieTea --allow-unauthenticated-identities --supported-login-providers graph.facebook.com=1965964200166925 --cognito-identity-providers ProviderName=cognito-idp.us-east-1.amazonaws.com/us-east-1_sRJMVrgoX,ClientId=2uauvigoq93bek7km88s587a74,ServerSideTokenCheck=false --query IdentityPoolId --output text

## Output: identity pool id
us-east-1:8f206702-374d-4721-aef4-98dcfe0e515d

# Create Two IAM Roles

Cognito Console

https://console.aws.amazon.com/cognito/home?region=us-east-1

Cognito_IndieTeaUnauth_Role
Cognito_IndieTeaAuth_Role

